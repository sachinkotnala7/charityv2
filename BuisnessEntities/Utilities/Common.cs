﻿using BuisnessEntities.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BuisnessEntities.Utilities
{

    public class UserInfoModel
    {
        [DataMember]
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[\+]*[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-??]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
        public string Password { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Required")]
        public string Id { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Required")]
        public string FullName { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Required")]
        public DateTime DOB { get; set; }
        
        public string Address { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Required")]
        public DateTime JoinedOn { get; set; }
        
        public int UserRoleId { get; set; }
    }

    public class UserDetailModel
    {
        public string Email { get; set; }
        public int UserRoleId { get; set; }
        public string EmployeeId { get; set; }

        public string FullName { get; set; }
        public string Username { get; set; }
        public DateTime JoinedOn { get; set; }
        public DateTime? RelievedOn { get; set; }
    }

    public class ExceptionModal
    {
        public Exception Exception { get; set; }
        public UserDetailModel User { get; set; }
        public string FormData { get; set; }
        public string QueryData { get; set; }
        public string RouteData { get; set; }
    }

    public class ExceptionReturnModal
    {
        public string ErrorID { get; set; }
        public string ErrorText { get; set; }
        public bool DatabaseLogStatus { get; set; }
    }

    public class ActionOutputBase
    {
        public ActionStatus Status { get; set; }
        public String Message { get; set; }
        //public List<String> Results { get; set; }
    }

    public class ActionOutput<T> : ActionOutputBase
    {
        public T Object { get; set; }
        public List<T> Results { get; set; }
    }

    public class ActionOutput : ActionOutputBase
    {
        public long ID { get; set; }
    }
    public class ApiActionOutput
    {
        public ActionStatus Status { get; set; }
        public String Message { get; set; }
        public Object JsonData { get; set; }
    }

    public class ApiActionPagingOutput
    {
        public ActionStatus Status { get; set; }
        public String Message { get; set; }
        public Object JsonData { get; set; }
        public long TotalRecords { get; set; }
    }

    //public class AuthorizationResponse : APIUser
    //{
    //    public AuthorizeErrorCode ErrorCode { get; set; }
    //}

    public class PagingResultBase : ActionOutputBase
    {
        //public int PageSize { get; set; }
        public long TotalResults { get; set; }
       
    }

    public class PagingResult<T> : PagingResultBase
    {
        public string Data { get; set; }
        public List<T> DataList { get; set; }
    }

    public class AddAttachment
    {
        public byte[] Stream { get; set; }

        public string MediaType { get; set; }
    }

    [DataContract]
    public class LoginModel
    {
        [DataMember]
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[\+]*[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-??]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataMember, StringLength(15, ErrorMessage = "Password should be less than 15 characters.")]
        public string Password { get; set; }
    }

    public class ResetPasswordModel
    {
       
        public string Email { get; set; }
        public string EmpId { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataMember, StringLength(15, ErrorMessage = "Password should be less than 15 characters.")]
        public string oldPassword { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataMember, StringLength(15, ErrorMessage = "Password should be less than 15 characters.")]
        public string NewPassword { get; set; }

    }

    public class ForgetPasswordModel
    {
        //[DataMember]
        //[Required(ErrorMessage = "Required")]
        //[RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[\+]*[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-??]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Invalid Email")]
        //public string Email { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataMember, StringLength(15, ErrorMessage = "Password should be less than 15 characters.")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataMember, StringLength(15, ErrorMessage = "Password should be less than 15 characters.")]
        public string ConfirmPassword { get; set; }
        public string Id { get; set; }

    }
    public class LoginRedirect
    {
        public int? RoleId { get; set; }
        public string RedirectUrl { get; set; }
    }

    public class InCompleteMessageModel
    {
        public string Message { get; set; }
    }

    public class ListItem
    {
        public ListItem()
        {
        }
        public ListItem(string val, string text)
        {
            Text = text;
            Value = val;
        }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class DocumentModel
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string SavedName { get; set; }
        public byte[] DownloadBinary { get; set; }
        public string Mime { get; set; }
    }
}
