﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Abstract
{
    public interface IErrorLogRepository
    {
        /// <summary>
        /// log the exception into database
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        string LogExceptionToDatabase(Exception ex, int userId);

        /// <summary>
        /// log the exception into file, if LogExceptionToDatabase fails
        /// </summary>
        /// <param name="ex"></param>        
        void LogExceptionToFile(String ex, int userId);
    }
}
