﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Entities
{
    public class ACNCModel
    {
        public double ABN { get; set; }

        public string CharityName { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string Website { get; set; }
        public string Contact { get; set; }
        public Nullable<double> Volunteer { get; set; }
        public string Description { get; set; }
        public string Activity { get; set; }
      

    }

    public class Graph
    {
        public string  Year { get; set; }
        public string CountOfPeople { get; set; }
     
    }
}
