﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Entities
{
    public class StudentInfo
    {
        public int Id { get; set; }
        [Required]
        public string StudentName { get; set; }
        public DateTime JoinedOn { get; set; }
    }
}
