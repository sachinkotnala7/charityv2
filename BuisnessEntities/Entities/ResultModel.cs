﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Entities
{
    public class ResultModel
    {
        public string ABN { get; set; }
        public string CharityName { get; set; }
        public string Address { get; set; }
        public string Main_Activity { get; set; }
        public string Phone_number { get; set; }
        public string website { get; set; }
        public string Description { get; set; }
        public string staff_casual { get; set; }
        public string Staff_Full_time { get; set; }
        public string Staff_part_time { get; set; }
        public string Staff_volunteer { get; set; }
        public string Government_grants { get; set; }
        public string Donation { get; set; }
        public string Revenue { get; set; }
        public string employee_expenses { get; set; }
        public string Interest_expenses { get; set; }
        public string other_expenses { get; set; }
        public string current_assets { get; set; }
        public string non_current_assets { get; set; }
        public string liabilities { get; set; }
        public string non_current_liabilities { get; set; }
    }
}
