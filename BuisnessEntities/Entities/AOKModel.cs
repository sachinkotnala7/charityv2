﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuisnessEntities.Entities
{
    public class AOKModel
    {
        public int cat { get; set; }
        public string Task { get; set; }

        public string Description { get; set; }
        public string ImagePath { get; set; }
    }
}
