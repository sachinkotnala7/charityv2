﻿using BuisnessEntities.Entities;
using BuisnessEntities.Utilities;
using DataAccessLayer.Model.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BuisnessEntities.Concrete
{
    public class ACNCRepository : IDisposable
    {
        private ACNCEntities Context;
        public ACNCRepository()
        {
            this.Context = new ACNCEntities();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public PagingResult<ACNCModel> GetCategoryData(FilterCriteriaModel model, bool allowPaging = true)
        {

            ObjectParameter output = new ObjectParameter("TotalResults", typeof(int));
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GetACNCDetailsByCategoryId(model.categoryid,
                        model.PageIndex,
                        model.PageSize,
                        model.SearchColumn,
                        output).ToList().OrderByDescending(x => x.Volunteer);

            ConfigurationRepository.SuspendExecutionStrategy = false;

            var dataCount = list.Select(x => new ACNCModel
            {
                ABN = x.ABN,
                CharityName = x.CharityName,
                Address = x.Address,
                Suburb = x.Suburb,
                Website = x.Website,
                Contact = x.Contact,
                Volunteer = x.Volunteer,
                Description = x.Description,
                Activity = x.Activity

            }).OrderByDescending(x => x.Volunteer).ToList();
            return new PagingResult<ACNCModel>()
            {
                Status = ActionStatus.Successfull,
                DataList = dataCount,
                TotalResults = Convert.ToInt32(output.Value)
            };
        }
        public PagingResult<ACNCModel> GetListOfACNCDetails(FilterCriteriaModel filterCriteria, bool allowPaging = true)
        {

            ObjectParameter output = new ObjectParameter("TotalResults", typeof(int));
            ConfigurationRepository.SuspendExecutionStrategy = true;

            //var list = Context.GetACNCDetails(filterCriteria.CareForId, filterCriteria.HelpForId, filterCriteria.FacilityId, filterCriteria.PageIndex, filterCriteria.PageSize, filterCriteria.SearchColumn, output).ToList();

            //ConfigurationRepository.SuspendExecutionStrategy = false;

            //var dataCount = list.Select(x => new ACNCModel
            //{
            //    CharityName = x.CharityName,
            //    Address = x.Address,


            //}).ToList();
            return new PagingResult<ACNCModel>()
            {
                Status = ActionStatus.Successfull,
                DataList = null,
                TotalResults = Convert.ToInt32(output.Value)
            };
        }

        public IEnumerable<SelectListItem> GetCareFor()
        {

            ConfigurationRepository.SuspendExecutionStrategy = true;

            //var list = Context.MostImportants.AsEnumerable().Select(x => new SelectListItem
            //{
            //    Text = x.CareFor,
            //    Value = x.Id.ToString(),
            //    Selected = false
            //});

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return null;
        }

        public List<SelectListItem> GetHelpFor(int CareForId)
        {

            ConfigurationRepository.SuspendExecutionStrategy = true;

            //var list = Context.HelpForCares.ToList().Where(x => x.MostImportantId == CareForId).Select(x => new SelectListItem
            //{
            //    Text = x.HelpCategory,
            //    Value = x.Id.ToString(),
            //    Selected = false
            //}).ToList();

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return null;
        }

        public List<SelectListItem> GetFacility(int HelpForId)
        {

            ConfigurationRepository.SuspendExecutionStrategy = true;

            //var list = Context.TypesOfFacilities.ToList().Where(x => x.HelpCareID == HelpForId).Select(x => new SelectListItem
            //{
            //    Text = x.Facility,
            //    Value = x.Id.ToString(),
            //    Selected = false
            //}).ToList();

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return null;
        }

        public void GetListOfOldPeople(out string CountOldPeopleList, out string InYearList)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GetOldAgeDetail().Select(x => new Graph
            {
                CountOfPeople = x.CountOfOldPeople.ToString(),
                Year = x.year.ToString()
            }).ToList();

            var countData = list.Select(x => x.CountOfPeople).ToList();
            CountOldPeopleList = string.Join(",", countData);

            var yearData = list.Select(x => x.Year).ToList();
            InYearList = string.Join(",", yearData);

            ConfigurationRepository.SuspendExecutionStrategy = false;

        }

        //Fecth details of Childcare assistance provided to the Glen Eira resident having child.

        public void GetChildCareAssistance(out string CountAssistance, out string InYearList)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GetChildcareAssistanceDetail().Select(x => new Graph
            {
                CountOfPeople = x.NoUnpaidAssistanceProvided.ToString(),
                Year = x.year.ToString(),

            }).ToList();

            var countData = list.Select(x => x.CountOfPeople).ToList();
            CountAssistance = string.Join(",", countData);

            var yearData = list.Select(x => x.Year).ToList();
            InYearList = string.Join(",", yearData);



            ConfigurationRepository.SuspendExecutionStrategy = false;

        }

        //Fecth details of Disability assistance provided to the Glen Eira disable resident.  

        public void GetDisabilityAssistance(out string CountDAssistance, out string D_A_Year)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GetDisabilityAssistanceDetail().Select(x => new Graph
            {
                CountOfPeople = x.NoUnpaidAssistanceProvided.ToString(),
                Year = x.year.ToString()
            }).ToList();

            var countData = list.Select(x => x.CountOfPeople).ToList();
            CountDAssistance = string.Join(",", countData);

            var yearData = list.Select(x => x.Year).ToList();
            D_A_Year = string.Join(",", yearData);

            ConfigurationRepository.SuspendExecutionStrategy = false;

        }

        //Fecth details of resident of Glen Eira who cannot speak english well.  

        public void GetSpokenEnglish(out string CountDAssistance, out string D_A_Year)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GetSpokenEnglishDetail().Select(x => new Graph
            {
                CountOfPeople = x.Total_spoken_english_not_well.ToString(),
                Year = x.year.ToString(),

            }).ToList();

            var countData = list.Select(x => x.CountOfPeople).ToList();
            CountDAssistance = string.Join(",", countData);

            var yearData = list.Select(x => x.Year).ToList();
            D_A_Year = string.Join(",", yearData);


            ConfigurationRepository.SuspendExecutionStrategy = false;

        }
        public void GlenEducation(out string CountEducation, out string E_Year)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;

            var list = Context.GlenEiraEducation().Select(x => new Graph
            {
                CountOfPeople = x.NoEducation.ToString(),
                Year = x.year.ToString(),

            }).ToList();

            var countData = list.Select(x => x.CountOfPeople).ToList();
            CountEducation = string.Join(",", countData);

            var yearData = list.Select(x => x.Year).ToList();
            E_Year = string.Join(",", yearData);


            ConfigurationRepository.SuspendExecutionStrategy = false;

        }
        public PagingResult<ACNCModel> SearchACNCDetails(FilterCriteriaModel filterCriteria)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;
            ObjectParameter output = new ObjectParameter("TotalResults", typeof(int));
            //var list = Context.SearchForCharity(filterCriteria.SearchColumn, filterCriteria.PageIndex, filterCriteria.PageSize, output).ToList();

            //ConfigurationRepository.SuspendExecutionStrategy = false;

            //var dataCount = list.Select(x => new ACNCModel
            //{
            //    CharityName = x.CharityName,
            //    Address = x.Address,
            //    // Town = x.Town_City,
            //    //charityWebsite = x.Charity_Website,
            //    //Size = x.Charity_Size

            //}).ToList();
            return new PagingResult<ACNCModel>()
            {
                Status = ActionStatus.Successfull,
                DataList = null,
                TotalResults = Convert.ToInt32(output.Value)
            };
        }

        public ResultModel GetCharityInfo(string abn)
        {


            ConfigurationRepository.SuspendExecutionStrategy = true;
            var list = Context.GetCharityInfo(abn).Select(x => new ResultModel
            {
                ABN = x.ABN.ToString(),
                CharityName = x.CharityName,
                Address = x.Address,
                Main_Activity = x.MainActivity,
                Phone_number = x.Phonenumber,
                website = x.website,
                Description = x.Description,
                staff_casual = x.staffcasual.HasValue ? x.staffcasual.Value.ToString() : "",
                Staff_Full_time = x.StaffFulltime.HasValue ? x.StaffFulltime.Value.ToString() : "",
                Staff_part_time = x.Staffparttime.HasValue ? x.Staffparttime.Value.ToString() : "",
                Staff_volunteer = x.Staffvolunteer.HasValue ? x.Staffvolunteer.Value.ToString() : "",
                Government_grants = x.Governmentgrants.HasValue ? x.Governmentgrants.Value.ToString() : "",
                Donation = x.Donation.HasValue ? x.Donation.Value.ToString() : "",
                Revenue = x.OtherRevenue.HasValue ? x.OtherRevenue.Value.ToString() : "",
                employee_expenses = x.employeeexpenses.HasValue ? x.employeeexpenses.Value.ToString() : "",
                Interest_expenses = x.Interestexpenses.HasValue ? x.Interestexpenses.Value.ToString() : "",
                other_expenses = x.otherexpenses.HasValue ? x.otherexpenses.Value.ToString() : "",
                current_assets = x.currentassets.HasValue ? x.currentassets.Value.ToString() : "",
                non_current_assets = x.noncurrentassets.HasValue ? x.noncurrentassets.Value.ToString() : "",
                liabilities = x.currentliabilities.HasValue ? x.currentliabilities.Value.ToString() : "",
                non_current_liabilities = x.noncurrentliabilities.HasValue ? x.noncurrentliabilities.Value.ToString() : ""

            }).ToList().FirstOrDefault();

            ConfigurationRepository.SuspendExecutionStrategy = false;
            return list;

            //var CharityName = list.Select(x => x.CharityName).ToList();
            //OCharityName = string.Join(",", CharityName);
        }


        public AOKModel GetAOK(int cat)
        {
            ConfigurationRepository.SuspendExecutionStrategy = true;
            var list = Context.GetActOfKindness(cat).Select(x => new AOKModel
            {
                cat = cat,
                Task = x.TASK,
                Description = x.Description,
                ImagePath = (cat == 1 ? "hero.jpg" : (cat == 2 ? "teen2.jpg" : (cat == 3 ? "Professional.jpg" : "old_love.jpg")))
            }).ToList().FirstOrDefault();
            ConfigurationRepository.SuspendExecutionStrategy = false;
            return list;
        }
    }
}
