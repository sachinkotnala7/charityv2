jQuery(document).ready(function ($) {
    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').fadeIn(300);
    }, function () {
        $(this).find('.dropdown-menu').fadeOut(300);
    }); 
     
    //$('#date').datepicker({
    //    format: "dd/mm/yyyy",
    //    autoclose: true
    //});

   
    $(".account-nav [href]").each(function () {
        if (this.href == location.href) {
            $(this).addClass("profileactive");
        }
    });

 	
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 1) {
            $("#header").addClass("background");
        } else {
            $("#header").removeClass("background");
        }
    });                                         


    $("#cat_p option:nth-child(2)").remove();


    /* ======= Fixed header when scrolled ======= 

	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 50) {
			$('#header').addClass('navbar-fixed-top');
		}
		else {
			$('#header').removeClass('navbar-fixed-top');
		}
	});*/
    /*bootstrap navigation adding on hover show event*/
    $('#header ul.nav li.dropdown').hover(
      function () {
          $(this).children('.dropdown-toggle').trigger('click');
          //.triggerHandler() Execute all handlers attached to an element for an event.
          
      }, function () {
        
      });
	
    $("#donateForm .advance-search-btn").on('click', function (e) { e.preventDefault(); })


 $('.dropdown-menu input').click(function (e) {
   e.stopPropagation(); //This will prevent the event from bubbling up and close the dropdown when you type/click on text boxes.
});

    $(".dropdown-menu").mouseleave(function () {
        $(".dropdown").removeClass("open");
    });
    $('#myModal').on('shown.bs.modal', function (e) {
        $('#loginTab').tabCollapse({
            tabsClass: 'hidden-xs',
            accordionClass: 'visible-xs'
        });
    });
   
 
    /*
    $('#lNGOalpha').tabCollapse({
        tabsClass: 'hidden-xs',
        accordionClass: 'visible-xs'
    });
    $('#lNGO-accordion').on('hidden.bs.collapse', function (e) {
        e.preventDefault();
        $('#lNGOalpha').tabCollapse({
            tabsClass: 'hidden-xs',
            accordionClass: 'visible-xs'
        });
        alert('Event fired on #' + e.currentTarget.id);
    })*/
  

    if ($(window).width() < 768) {
        /*$('#myTab,#searchfoundation,#monthlygivetab,#lNGO,#team,#loginTab').tabCollapse({
        });*/
        
        $('#search .item').addClass('list-group-item');
        $('#search .item').removeClass('grid-group-item');       
       
    } else {           
        $('#search .item').addClass('grid-group-item');
        $('#search .item').removeClass('list-group-item');
    }
   
    $('#myaccountCarousel').carousel();
   
    
    //addWrapper();
    //$(window).resize(function () { addWrapper(); });
    


    //function addWrapper()
    //{
        
    //    if ($(window).width() > 767 && $(window).width() < 1200) {            
    //        $('.one, .two').wrapAll($('<div>').addClass('wrap'));
    //        $('.three, .four').wrapAll($('<div>').addClass('wrap'));
    //    }
    //    else {
    //        $('.wrap').removeClass('wrap');
    //        $('.wrap').removeClass('wrap');
    //    }
    //}

    
    $('#donateForm .advance-search-btn').click(function () {
        $(this).preventDefault()
    });
    // show-pagination
    $('.toggle-pagination').click(function (f) {
        $(this).next('.searchpagi .pagination').slideToggle();
        $(this).toggleClass('active');
        f.preventDefault()
    });

	/*grid to list view switch*/
	$('#list').click(function (event) {
		event.preventDefault();
		$(this).addClass('active').next('#grid').removeClass('active');
		$('#search .item').addClass('list-group-item');
		$('#search .item').removeClass('grid-group-item');
		/*auto height in list view*/
		$('#search').each(function () {
			$(this).find('.item').height('auto');
		});
	});
	$('#grid').click(function (event) {
		event.preventDefault();
		$(this).addClass('active').prev('#list').removeClass('active');
		$('#search .item').removeClass('list-group-item');
		$('#search .item').addClass('grid-group-item');
		/*equal height in grid view*///http://bootsnipp.com/snippets/featured/list-grid-view
		$('#search').each(function () {
			var highestBox = 0;
			$(this).find('.item').each(function () {
				if ($(this).height() > highestBox) {
					highestBox = $(this).height();
				}
			})
			$(this).find('.item').height(highestBox);
		});
	});

    try {
        $("#donation-amt").slider({});  
    }
    catch (Exception) { }

    /*bootstrap tooltip*/
	$('[data-toggle="popover"]').popover();
	

    /*bootstrap modal*/
	$(".pop .btn,.pop .regipop").click(function () {
	    $("#myModal").modal('show'); 	   
	});


	/*sticky sec dynamic height
	$('.donatehead').click(function () {
		var stickySecHeight = $(window).height() - $('#header').height();
		if ($('#donateForm').hasClass('in')) {
			$('#sticky-sec').height('auto');
		}
		else {
			$('#sticky-sec').height(stickySecHeight);
		}
	}); */
	/*stick on page scroll
	var stickySec = $('#sticky-sec');
	var headerHeight = $("#header").height() + 3;
	var offsetTop = stickySec.offset().top - headerHeight;

	function scroll() {
		if ($(window).scrollTop() >= offsetTop) {
			$('#sticky-sec').css({
				'position': 'fixed',
				'top': '80px'
			});
		} else {
			$('#sticky-sec').css({
				'position': 'absolute',
				'top': 'auto'
			});
		}
	}
	document.onscroll = scroll; 
    */
	/*donate modal in mobile*/
	var donatePanel = $('.donate-panel').html();
	$('.donate-btn').on('click', function () {
		$('#donateModal .modal-body').html(donatePanel);
		$('#donateModal').modal('show');
		$('#donateModal').find('#donateForm').collapse('show');
		$('#donateModal').find('#advanceSearchForm').collapse('show');
	});

  
    /* ======= Scrollspy ======= */
    $('body').scrollspy({ target: '#header', offset: 400});
   
    /* ======= ScrollTo ======= */
    $('a.scrollto').on('click', function(e){
        
        //store hash
        var target = this.hash;
                
        e.preventDefault();
        
		$('body').scrollTo(target, 800, {offset: -70, 'axis':'y', easing:'easeOutQuad'});
        //Collapse mobile menu after clicking
		if ($('.navbar-collapse').hasClass('in')){
			$('.navbar-collapse').removeClass('in').addClass('collapse');
		}
		
    });

     
    $(".imp").hide();
    //$("select").change(function () {
    //    $(this).find("option:selected").each(function () {
    //        if ($(this).attr("value") == "INRC") {
    //            $(".imp").hide();
    //            $(".contactno").show();
    //        }
    //        else if ($(this).attr("value") == "OTH") {
    //            $(".imp").hide();
    //            $(".currency,.national").show();
    //        }
    //        else if ($(this).attr("value") == "NRI") {
    //            $(".imp").hide();
    //            $(".pssportno").show();
    //        }
    //        else if ($(this).attr("value") == "USD", "GBP") {
    //            $(".imp").hide();
    //        }
    //    });
    //}).change();  
 });

function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');   
}

$('#myTab,#searchfoundation,#monthlygivetab,#lNGO,#team,#myTabNGO,#lNGOalpha,#searchfoundationtab,#loginTab').tabCollapse({
});

/*give your li id of test and it fire on mouseover to refresh the tabs*/


$('#team > li > a').hover(function () {
    $(this).tab('show');
});

$('#team > li ').hover(function () {
    if ($(this).hasClass('hoverblock'))
        return;
    else
        $(this).find('a').tab('show');
});


$('#team > li').find('a').click(function () {
    $(this).parent()
        .siblings().addClass('hoverblock');
});

$('#team > li > a').hover(function () {
    $(this).trigger('click'); 
});

$('#lNGO').tabCollapse({
    tabsClass: 'hidden-xs',
    accordionClass: 'visible-xs'
});
$(document).on("shown.bs.collapse shown.bs.tab", ".panel-collapse, a[data-toggle='tab']", function (e) {
    //alert('either tab or collapse opened - check arguments to distinguish ' + e);
    $('#lNGO').tabCollapse({
        tabsClass: 'hidden-xs',
        accordionClass: 'visible-xs'
    });
});


setCarouselHeight('#wwd-carousel');

function setCarouselHeight(id) {
    var slideHeight = [];
    $(id + ' .item').each(function () {
        // add all slide heights to an array
        slideHeight.push($(this).height());
    });

    // find the tallest item
    max = Math.max.apply(null, slideHeight);

    // set the slide's height
    $(id + ' .carousel-content').each(function () {
        $(this).css('height', max + 'px');
    });
}

//$('#accordion2').on('hidden.bs.collapse', toggleChevron);
//$('#accordion2').on('shown.bs.collapse', toggleChevron);
//$("#accordion2.collapse").collapse();
//$('#accordion2').collapse({ hide: true })
//$('#collapseOne').collapse("hide");


/*sticky div on page scroll - https://css-tricks.com/scrollfollow-sidebar/
$(function () {
    var offset = $("#sticky-sec").offset();
    var headerHeight = $("#header").height() + 3;
    var topPadding = headerHeight;
    $(window).scroll(function () {
        if ($(window).scrollTop() > offset.top) {
            $("#sticky-sec").stop().animate({
            	//marginTop: $(window).scrollTop() - offset.top + topPadding
            	marginTop: $(window).scrollTop() - offset.top + topPadding
            });
        } else {
            $("#sticky-sec").stop().animate({
                marginTop: 0
            });
        };
    });
});
*/

//function header_height() {
//    var header_height = jQuery(".headerwrapper header").innerHeight() + 2;
//    //alert(header_height);
//    jQuery(".headerwrapper").css('height', header_height);
//}

//jQuery(document).ready(function () {
//    header_height();
//});

//jQuery(window).resize(function () {
//    header_height();
//});