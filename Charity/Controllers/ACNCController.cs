﻿using BuisnessEntities.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuisnessEntities.Concrete;
using DCCA.Controllers;

namespace Charity.Controllers
{
    public class ACNCController : BaseController
    {
       
        public ACNCController() : base()
        {
            
        }
        // GET: ACNC
        public ActionResult GetACNCDetails(FilterCriteriaModel criteria)
        {
            var result = new ACNCRepository().GetListOfACNCDetails(criteria);
            string view = RenderRazorViewToString("_LoadResults", result.DataList);
            result.Data = view;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCareFor()
        {
            var result = new ACNCRepository().GetCareFor().ToList();
            string view = RenderRazorViewToString("_CareFor", result);
            return Json(new ActionOutput<string>{
                Object = view,
                Status = BuisnessEntities.Entities.ActionStatus.Successfull
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHelpFor(int CareForId)
        {
            var result = new ACNCRepository().GetHelpFor(CareForId);
            string view = RenderRazorViewToString("_HelpForData", result);
            return Json(new ActionOutput<string>
            {
                Object = view,
                Status = BuisnessEntities.Entities.ActionStatus.Successfull
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFacilities(int HelpForId)
        {
            var result = new ACNCRepository().GetFacility(HelpForId);
            string view = RenderRazorViewToString("_Facilities", result);
            return Json(new ActionOutput<string>
            {
                Object = view,
                Status = BuisnessEntities.Entities.ActionStatus.Successfull
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Volunteer()
        {
            try
            {
                string CountOldPeopleList = string.Empty;
                string InYearList = string.Empty;
                string CountChildAssistance = string.Empty;
                string CAssistanceYearList = string.Empty;
                string ChildCareList = string.Empty;
                string CountDisabilityAssistance = string.Empty;
                string DAssistanceYearList = string.Empty;
                string CountEngSpeak = string.Empty;
                string EngIssue = string.Empty;

                new ACNCRepository().GetListOfOldPeople(out  CountOldPeopleList, out InYearList);
                ViewBag.PeopleCount_List = CountOldPeopleList.Trim();
                ViewBag.Year_List = InYearList.Trim();

                new ACNCRepository().GetChildCareAssistance(out CountChildAssistance, out CAssistanceYearList);
                ViewBag.CountChildAssistance_List = CountChildAssistance.Trim();
                ViewBag.CAssistanceYear_List = CAssistanceYearList.Trim();
                

                new ACNCRepository().GetDisabilityAssistance(out CountDisabilityAssistance, out DAssistanceYearList);
                ViewBag.CountDisabilityAssistance_List = CountDisabilityAssistance.Trim();
                ViewBag.DAssistanceYear_List = DAssistanceYearList.Trim();

                new ACNCRepository().GetSpokenEnglish(out CountEngSpeak,out InYearList);
                ViewBag.CountEngSpeak_List = CountEngSpeak.Trim();
                ViewBag.EngIssue_List = EngIssue.Trim();

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SearchForACNCDetails(FilterCriteriaModel filter)
        {
            var result = new ACNCRepository().SearchACNCDetails(filter);
            string view = RenderRazorViewToString("_LoadResults", result.DataList);
            result.Data = view;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IssuesInGE()   
        {
            try
            {
                string CountOldPeopleList = string.Empty;
                string InYearList = string.Empty;
                string CountChildAssistance = string.Empty;
                string CAssistanceYearList = string.Empty;
                string CChildCare_List = string.Empty;
                string CountDisabilityAssistance = string.Empty;
                string DAssistanceYearList = string.Empty;
                string CountEngSpeak = string.Empty;
                string EngIssue_Year = string.Empty;
                string CountEducation = string.Empty;
                string Education_Year = string.Empty;

                new ACNCRepository().GetListOfOldPeople(out CountOldPeopleList, out InYearList);
                ViewBag.PeopleCount_List = CountOldPeopleList.Trim();
                ViewBag.Year_List = InYearList.Trim();

                new ACNCRepository().GetChildCareAssistance(out CountChildAssistance, out CAssistanceYearList);
                ViewBag.CountChildAssistance_List = CountChildAssistance.Trim();
                ViewBag.CAssistanceYear_List = CAssistanceYearList.Trim();
                

                new ACNCRepository().GetDisabilityAssistance(out CountDisabilityAssistance, out DAssistanceYearList);
                ViewBag.CountDisabilityAssistance_List = CountDisabilityAssistance.Trim();
                ViewBag.DAssistanceYear_List = DAssistanceYearList.Trim();

                new ACNCRepository().GetSpokenEnglish(out CountEngSpeak, out EngIssue_Year);
                ViewBag.CountEngSpeak_List = CountEngSpeak.Trim();
                ViewBag.English_Year = EngIssue_Year.Trim();

                new ACNCRepository().GlenEducation(out CountEducation, out Education_Year);
                ViewBag.CountEducation = CountEducation.Trim();
                ViewBag.Education_Year = Education_Year.Trim();

                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult test()
        {
            return View();
        }
    }
   
}