﻿using BuisnessEntities.Concrete;
using BuisnessEntities.Entities;
using BuisnessEntities.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Charity.Controllers
{
    public class DonationController : BaseController
    {
        public DonationController() : base()
        {

        }
        // GET: Donation
        public ActionResult Donate()
        {
            FilterCriteriaModel searchModel = new FilterCriteriaModel();
            searchModel.PageSize = 10;
            ListModel<ACNCModel> model = new ListModel<ACNCModel>();
            model.PagingResult = new ACNCRepository().GetListOfACNCDetails(searchModel);
            model.FilterCriteriaModel = searchModel;
            return View(model);
        }
        public ActionResult Category(int Category = 0)
        {
            
                if (Category == 0)
                {
                    ViewBag.Heading = "all the categories";
                }
                if (Category == 1)
                {
                    ViewBag.Heading = "Animals";
                }
                if (Category == 2)
                {
                    ViewBag.Heading = "Environment";
                }
                if (Category == 3)
                {
                    ViewBag.Heading = "Disability";
                }
                if (Category == 4)
                {
                    ViewBag.Heading = "Religion";
                }
                if (Category == 5)
                {
                    ViewBag.Heading = "Aged People";
                }
                if (Category == 6)
                {
                    ViewBag.Heading = "Social and Public welfare";
                }
                if (Category == 7)
                {
                    ViewBag.Heading = "Health";
                }
                if (Category == 8)
                {
                    ViewBag.Heading = "Education";
                }
                if (Category == 9)
                {
                    ViewBag.Heading = "Homeless People";
                }
                if (Category == 10)
                {
                    ViewBag.Heading = "Childcare";
                }
                FilterCriteriaModel searchModel = new FilterCriteriaModel();
                searchModel.PageSize = 6;
                searchModel.categoryid = Category;
                ListModel<ACNCModel> model = new ListModel<ACNCModel>();
                model.PagingResult = new ACNCRepository().GetCategoryData(searchModel);
                model.FilterCriteriaModel = searchModel;
                return View(model);
           
           
        }
        public ActionResult SearchResult()
        {

            FilterCriteriaModel searchModel = new FilterCriteriaModel();
            searchModel.PageSize = 10;
            
            ListModel<ACNCModel> model = new ListModel<ACNCModel>();
            model.PagingResult = new ACNCRepository().GetCategoryData(searchModel);
            model.FilterCriteriaModel = searchModel;
            return View(model);
        }

        public ActionResult GetACNCDetails(FilterCriteriaModel model)
        {
            var result = new ACNCRepository().GetCategoryData(model);
            string view = RenderRazorViewToString("_LoadResultTest", result.DataList);
            result.Data = view;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Resultpage()
        {
            return View();
        }

        public ActionResult CharityInfo(string ABN)
        {
           ResultModel model= new ACNCRepository().GetCharityInfo(ABN);
           return View("Resultpage",model);
        }
    }
}