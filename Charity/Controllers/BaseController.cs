﻿using BuisnessEntities.Abstract;
using BuisnessEntities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuisnessEntities.Entities;
using BuisnessEntities.Utilities;
using System.Web.Routing;
using System.IO;
using System.Web.Security;
using System.Web.Script.Serialization;

namespace Charity.Controllers
{
    public class BaseController : Controller
    {
        
        private readonly IErrorLogRepository ErrorLogRepository;

        public string Action;
        public string Controller;
        public string BaseUrl;
        protected UserModel LOGGEDIN_USER { get; set; }
        protected string SiteCulture { get; set; }

        public BaseController()
        {
            this.ErrorLogRepository = new ErrorLogRepository();
        }

        /// <summary>
        /// This will check validation error on action executing
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            List<string> errors = new List<string>();
            //Get Data Against a specific RoleName
            if (!ModelState.IsValid)
            {
                var message = string.Join(", ", ModelState.Values
                                    .Where(m => m.Errors.Count() > 0)
                                    .SelectMany(m => m.Errors)
                                    .Select(e => e.ErrorMessage));

                message += string.Join(", ", ModelState.Values
                                    .Where(m => m.Errors.Count() > 0)
                                    .SelectMany(m => m.Errors)
                                    .Where(e => e.Exception != null)
                                    .Select(m => m.Exception.Message));

                //foreach (ModelState modelState in ViewData.ModelState.Values)
                //{
                //    foreach (ModelError error in modelState.Errors)
                //    {
                //        errors.Add(error.ErrorMessage);
                //    }
                //}
                if (Request.IsAjaxRequest()) filterContext.Result = Json(new ActionOutput
                {
                    Status = ActionStatus.Error,
                    Message = "Validation Error: " + message
                }, JsonRequestBehavior.AllowGet);

                //This needs to be changed to redirect the control to an error page.
                else filterContext.Result = null;
            }
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        /// <summary>
        /// This will be used to check user authorization
        /// </summary>
        /// <param name="filter_context"></param>
        protected override void OnAuthorization(AuthorizationContext filter_context)
        {
            BaseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            SetActionName(filter_context.ActionDescriptor.ActionName, filter_context.ActionDescriptor.ControllerDescriptor.ControllerName);

            SiteCulture = UserHelper.GetCulture();

            HttpCookie auth_cookie = Request.Cookies[Cookies.AuthorizationCookie];

            #region If auth cookie is present
            if (auth_cookie != null)
            {
                #region Renew Auth_Cookie
                auth_cookie.Expires = DateTime.Now.AddMinutes(Constants.SessionTimeoutMinutes);
                System.Web.HttpContext.Current.Response.Cookies.Add(auth_cookie);
                #endregion

                #region If Logged User is null
                if (LOGGEDIN_USER == null)
                {
                    try
                    {
                        FormsAuthenticationTicket auth_ticket = FormsAuthentication.Decrypt(auth_cookie.Value);
                        LOGGEDIN_USER = new JavaScriptSerializer().Deserialize<UserModel>(auth_ticket.UserData);
                        System.Web.HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(auth_ticket), null);
                        ViewBag.User = LOGGEDIN_USER;
                    }
                    catch (Exception exc)
                    {
                        if (auth_cookie != null)
                        {
                            auth_cookie.Expires = DateTime.Now.AddDays(-30);
                            Response.Cookies.Add(auth_cookie);
                            filter_context.Result = RedirectToAction("login", "home");
                            LogExceptionToDatabase(exc);
                        }
                    }
                }
                #region abc
                if (LOGGEDIN_USER.UserRoleId == 4 && Action == "users")
                {
                    if (!Request.IsAjaxRequest()) filter_context.Result = RedirectToAction("Unauthorized", "admin");
                    else filter_context.Result = Json(new ActionOutput
                    {
                        Status = ActionStatus.Error,
                        Message = "Unauthorized Access"
                    }, JsonRequestBehavior.AllowGet);
                }
                #endregion

                #endregion
                ViewBag.User = LOGGEDIN_USER;
            }
            #endregion

            #region if authorization cookie is not present and the action method being called is not marked with the [Public] attribute
            //else if (!filter_context.ActionDescriptor.GetCustomAttributes(typeof(Public), false).Any())
            //{
            //    if (!Request.IsAjaxRequest()) filter_context.Result = RedirectToAction("login", "home", new { returnUrl = Server.UrlEncode(Request.RawUrl) });
            //    else filter_context.Result = Json(new ActionOutput
            //    {
            //        Status = ActionStatus.LoggedOut,
            //        Message = "Your session was expired, Redirecting to login page."
            //    }, JsonRequestBehavior.AllowGet);
            //}
            #endregion
            #region if authorization cookie is not present and the action method being called is marked with the [Public] attribute
            else
            {
                LOGGEDIN_USER = new UserModel();
                ViewBag.User = LOGGEDIN_USER;
            }
            #endregion

        }


        public ActionResult Unauthorized()
        {
            return View();
        }

        /// <summary>
        /// This will be used to handle exceptions 
        /// </summary>
        /// <param name="filter_context"></param>
        protected override void OnException(ExceptionContext filter_context)
        {

            filter_context.ExceptionHandled = true;
            var error_id = "";
            var msg = string.Empty;
            if (filter_context.Exception.Message.ToLower().StartsWith("the anti-forgery token could not be decrypted"))
            {
                msg = "Server cannot process fake/forgery requests";
            }
            else
                error_id = LogExceptionToDatabase(filter_context.Exception);//log exception in database


            if (filter_context.Exception.GetType() == typeof(HttpRequestValidationException)) msg = "HTML tags or malicious characters are not allowed";

            //redirect control to ErrorResultJson action method if the request is an ajax request
            ViewBag.Message = (string.IsNullOrWhiteSpace(msg) ? "Error : " + error_id : msg) + ". Please contact the helpdesk.";

            if (Request.IsAjaxRequest()) filter_context.Result = Json(new ActionOutput
            {
                Status = ActionStatus.Error,
                Message = (string.IsNullOrWhiteSpace(msg) ? "Error : " + error_id : msg) + ". Please contact the helpdesk."
            }, JsonRequestBehavior.AllowGet);

            //This needs to be changed to redirect the control to an error page.
            else filter_context.Result = RedirectToAction("Error", new { id = error_id });

            base.OnException(filter_context);
        }

        /// <summary>
        /// log exception to database
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private String LogExceptionToDatabase(Exception ex)
        {
            try
            {
                //Log exception in database
                var result = ErrorLogRepository.LogExceptionToDatabase(ex, (LOGGEDIN_USER != null ? LOGGEDIN_USER.UserId : 0));
                return result;
            }
            catch (Exception)
            {
                LogExceptionToFile(ex.ToString(), (LOGGEDIN_USER != null ? LOGGEDIN_USER.UserId.ToString() : "0"));
                return "0";
            }
        }

        /// <summary>
        /// Log exception to text file on server
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="ex_message"></param>
        private void LogExceptionToFile(String ex, String ex_message)
        {
            System.IO.StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(Server.MapPath("~/ErrorLog.txt"), true);
                sw.WriteLine(ex_message);
                sw.WriteLine("http://jsonformat.com/");
                sw.WriteLine(ex); sw.WriteLine(""); sw.WriteLine("");
            }
            catch { }
            finally { sw.Close(); }
        }


        protected virtual void CreateCustomAuthorisationCookie(String user_name, Boolean is_persistent, String custom_data)
        {
            HttpCookie cookie = null;
            try
            {
                FormsAuthenticationTicket auth_ticket =
                new FormsAuthenticationTicket(
                    1, user_name,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(Constants.SessionTimeoutMinutes),
                    is_persistent, custom_data, ""
                );
                String encrypted_ticket_ud = FormsAuthentication.Encrypt(auth_ticket);
                cookie = new HttpCookie(Cookies.AuthorizationCookie, encrypted_ticket_ud);
                //if (is_persistent) cookie.Expires = auth_ticket.Expiration;
                cookie.Expires = DateTime.Now.AddMinutes(Constants.SessionTimeoutMinutes);
                System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch (Exception exc)
            {
                Utility.LogExceptionToFile(exc.ToString(), user_name);
            }

        }


        protected virtual void CreateCookie(String cookieName, Boolean is_persistent, String custom_data)
        {
            FormsAuthenticationTicket auth_ticket =
                new FormsAuthenticationTicket(
                    1, cookieName,
                    DateTime.Now,
                    DateTime.Now.AddDays(7),
                    is_persistent, custom_data, ""
                );
            String encrypted_ticket_ud = FormsAuthentication.Encrypt(auth_ticket);
            HttpCookie cookie = new HttpCookie(cookieName, encrypted_ticket_ud);
            if (is_persistent) cookie.Expires = auth_ticket.Expiration;
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /////// <summary>
        /////// used to create user authorization cookie after login
        /////// </summary>
        /////// <param name="user_name"></param>
        /////// <param name="is_persistent"></param>
        /////// <param name="custom_data"></param>
        ////protected virtual void CreateCustomAuthorisationCookie(String user_name, Boolean is_persistent, String custom_data, out HttpCookie cookie)
        ////{
        ////    FormsAuthenticationTicket auth_ticket =
        ////        new FormsAuthenticationTicket(
        ////            1, user_name,
        ////            DateTime.Now,
        ////            DateTime.Now.AddHours(12),
        ////            is_persistent, custom_data, ""
        ////        );
        ////    String encrypted_ticket_ud = FormsAuthentication.Encrypt(auth_ticket);
        ////    cookie = new HttpCookie(Cookies.AuthorizationCookie, encrypted_ticket_ud);
        ////    if (is_persistent) cookie.Expires = auth_ticket.Expiration;
        ////    System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        ////}

        /// <summary>
        /// used to update user authorization cookie after login
        /// </summary>
        /// <param name="user_name"></param>
        /// <param name="is_persistent"></param>
        /// <param name="custom_data"></param>
        protected virtual void UpdateCustomAuthorisationCookie(String custom_data)
        {
            var cookie = Request.Cookies[Cookies.AuthorizationCookie];
            FormsAuthenticationTicket authTicketExt = null;
            try
            {
                authTicketExt = FormsAuthentication.Decrypt(cookie.Value);
            }
            catch (Exception ex)
            {

            }

            FormsAuthenticationTicket auth_ticket =
            new FormsAuthenticationTicket(
                1, authTicketExt.Name,
                authTicketExt.IssueDate,
                authTicketExt.Expiration,
                authTicketExt.IsPersistent, custom_data, String.Empty
            );
            String encryptedTicket = FormsAuthentication.Encrypt(auth_ticket);
            cookie = new HttpCookie(Cookies.AuthorizationCookie, encryptedTicket);
            if (authTicketExt.IsPersistent) cookie.Expires = auth_ticket.Expiration;
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// this will be used to render a view as a string 
        /// </summary>
        /// <param name="view_name"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        protected string RenderRazorViewToString(string view_name, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, view_name);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// Will be used to logout from the application
        /// </summary>
        /// <returns></returns>
        [HttpGet] /*[Public]*/
        public virtual ActionResult LogOut()
        {
            HttpCookie auth_cookie = Request.Cookies[Cookies.AuthorizationCookie];
            if (auth_cookie != null)
            {
                auth_cookie.Expires = DateTime.Now.AddDays(-30);
                Response.Cookies.Add(auth_cookie);
            }

            return Redirect(Url.Action("Login", "home"));
        }
        /// <summary>
        /// This will be used to set action name
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        private void SetActionName(string actionName, string controllerName)
        {
            ViewBag.Controller = Controller = controllerName.ToLower();
            ViewBag.Action = Action = actionName.ToLower();
        }

        /// <summary>
        /// Setting Client side TimeZone Offset Cookie
        /// </summary>
        /// <param name="TimeZoneOffsetValue"></param>
        //[Public]
        public void TimezoneOffset(int TimeZoneOffsetValue)
        {
            HttpCookie cookie = new HttpCookie(Cookies.TimezoneOffset, TimeZoneOffsetValue.ToString());
            Response.Cookies.Add(cookie);
        }
        /// <summary>
        /// Convert UTC to Client Timezone value
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        //[Public]
        public DateTime ToClientTime(DateTime dateTime)
        {
            HttpCookie cookie = Request.Cookies[Cookies.TimezoneOffset];
            if (cookie != null)
                return dateTime.AddMinutes(Convert.ToInt32(cookie.Value));
            return dateTime;
        }
        //[Public]
        public ActionResult SetTimeZoneOffset() { return View(); }
        //[Public]
        public ActionResult Exception(string id)
        {
            return View(id);
        }
        //[Public]
        public ActionResult Error(string id)
        {
            return View(viewName: "Message", model: id);
        }

    }

}