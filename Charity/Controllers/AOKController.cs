﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuisnessEntities.Concrete;
using BuisnessEntities.Entities;
using BuisnessEntities.Utilities;
using System.Web.Mvc;

namespace Charity.Controllers
{
    public class AOKController : BaseController
    {
        // GET: AOK
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AOK()
        {
            return View();
        }

        public ActionResult ShowDetails(int CatId = 0)
        {
            var result = new ACNCRepository().GetAOK(CatId);
            string view = RenderRazorViewToString("_AOKModelPopup", result);
            return Json(view, JsonRequestBehavior.AllowGet);
        }

    }
}